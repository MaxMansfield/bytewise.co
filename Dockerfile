FROM dock0/arch
MAINTAINER Max Mansfield <max.m.mansfield@gmail.com>

# root password
RUN echo "root:toor" | chpasswd

# Setup pacman with fastest US mirrors
RUN curl "https://www.archlinux.org/mirrorlist/?country=US&protocol=http&protocol=https&ip_version=4" > /etc/pacman.d/mirrorlist.new
RUN sed -i "s/^#Server/Server/" /etc/pacman.d/mirrorlist.new
RUN rankmirrors -n 6 /etc/pacman.d/mirrorlist.new > /etc/pacman.d/mirrorlist

# Install updates and requisite packages
RUN pacman --noconfirm -Syyu base-devel git go nodejs

# Create a dev user w/ passwordless sudo privileges
RUN useradd -m -G wheel -s /bin/bash dev && echo "dev:dpass" | chpasswd
RUN echo "dev ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/dev

USER dev
WORKDIR /home/dev

# Setup golang and global node path
RUN mkdir -p ~/.node_modules ~/go/{src/bitbucket.org/maxmansfield/bytewise.co,pkg,bin}
RUN echo "export $GOPATH=$HOME/go" >> ~/.bash_profile
RUN echo "export PATH=$PATH:/$GOPATH/bin" >> ~/.bash_profile
run echo "export npm_config_prefix=~/.node_modules" >> ~/.bash_profile
RUN echo "export PATH=$PATH:$npm_config_prefix/bin" >> ~/.bash_profile


RUN git clone https://aur.archlinux.org/package-query.git /home/dev/package-query
RUN cd package-query && makepkg -si --noconfirm
RUN git clone https://aur.archlinux.org/yaourt.git /home/dev/yaourt
RUN cd yaourt && makepkg -si --noconfirm
RUN rm -r yaourt package-query

# Get yarn
RUN yaourt --noconfirm -S yarn

# The repository should be mounted here
WORKDIR /home/dev/go/src/bitbucket.org/maxmansfield/bytewise.co
RUN git clone https://bitbucket.org/MaxMansfield/bytewise.co.git .
# install node modules
RUN yarn install

# build the ./dist folder for the StartServer
RUN yarn build

RUN go get
RUN go build server.go
RUN chmod +x ./server

EXPOSE 8080
CMD ["./server"]
