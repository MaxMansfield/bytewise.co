// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'foot_test.js [components/Foot]': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL
    const fnav = "div#foot-nav"
    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .resizeWindow(768,1024)
      .assert.elementPresent(fnav)
      .assert.elementPresent(fnav + ' ul li a')
      .end()

  }
}
