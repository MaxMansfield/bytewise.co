// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'navbar_test.js [components/Navbar]': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL
    const ntoggle = '.nav-toggle'
    const blogo = 'img#bytws-logo'

    browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
      .assert.elementPresent('nav')
      .assert.elementPresent(ntoggle)
      .assert.elementPresent(blogo)
      // Test Mobile size
      .resizeWindow(768,1024)
      .waitForElementVisible(ntoggle, 5000)
      .assert.elementPresent(blogo)
      .assert.cssClassNotPresent(ntoggle,'is-active')
      .click('span' + ntoggle)
      .assert.cssClassPresent(ntoggle,'is-active')
      .assert.visible(ntoggle)
      .end()

  }
}
