import Vue from 'vue'
import Router from 'vue-router'

import Foot from 'src/components/Foot'
import router from 'src/router'

describe('components/Foot.vue', () => {
  Vue.use(Router)
  const Constructor = Vue.extend(Foot)
  const vm = new Constructor({
    router: router
  }).$mount()

  it('should have a homeLinks array', () => {
    expect(vm.$data.homeLinks).to.exist
  })
  it('should have more than one element in the homeLinks array', () => {
    expect(vm.$data.homeLinks.length).to.be.above(1)
  })
  it('should have a blogLinks array', () => {
    expect(vm.$data.blogLinks).to.exist
  })
  it('should have more than one element in the blogLinks array', () => {
    expect(vm.$data.blogLinks.length).to.be.above(1)
  })
})
