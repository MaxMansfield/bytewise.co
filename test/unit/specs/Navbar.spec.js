import Vue from 'vue'
import Router from 'vue-router'
import Nav from 'src/components/Navbar'
import router from 'src/router'

describe('components/Navbar.vue', () => {
  Vue.use(Router)
  const Constructor = Vue.extend(Nav)
  const vm = new Constructor({
    router: router
  }).$mount()

  it('should have a logo', () => {
    expect(vm.$el.querySelector('img#bytws-logo'))
  })

  it('should have an isMenuActive bool', () => {
    expect(typeof Nav.data().isMenuActive).to.eql(typeof true)
  })

  it('should start with isMenuActive == false', () => {
    expect(vm.$data.isMenuActive).to.be.false
  })

  it('should have a toggleMenu() function', () => {
    expect(vm.toggleMenu()).to.exist
  })

  it('should toggle isMenuActive with toggleMenu()', () => {
    expect(vm.$data.isMenuActive).to.be.true
  })
})
