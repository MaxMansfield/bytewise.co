package main

import "github.com/labstack/echo"

func main() {
	e := echo.New()

	// serve the site
	e.File("/", "./dist/index.html")
	e.Static("/static", "./dist/static")
	e.Logger.Fatal(e.Start(":8080"))
}
