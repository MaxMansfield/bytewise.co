import Vue from 'vue'
import Router from 'vue-router'

import Home from 'components/Home'
import About from 'components/Home/about'
import Hire from 'components/Home/hire'
import Join from 'components/Home/join'
import Team from 'components/Home/team'
import Portfolio from 'components/Home/portfolio'

import Blog from 'components/Blog'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      name: 'About',
      path: 'about',
      component: About
    },
    {
      name: 'Hire',
      path: 'hire',
      component: Hire
    },
    {
      name: 'Join',
      path: 'join',
      component: Join
    },
    {
      name: 'Team',
      path: 'team',
      component: Team
    },
    {
      name: 'Portfolio',
      path: 'portfolio',
      component: Portfolio
    },
    {
      path: 'blog',
      name: 'Blog',
      component: Blog
    }
  ]
})
